package com.luxoft.bankapp.service.bank;

import com.luxoft.bankapp.domain.bank.Bank;
import com.luxoft.bankapp.domain.bank.Client;
import com.sun.swing.internal.plaf.synth.resources.synth_sv;

/**
 * Created by 2 on 11.03.15.
 */
public class BankService {

    public static void addClient(Bank bank, Client client){

        Client[] clients = bank.getClients();
        for(int i = 0; i < clients.length; i++){
            if( clients[i] == null)
            {
                clients[i] = client;
                break;
            }
        }
    }

    public static void printMaximumAmountToWithdraw(Bank bank){
        System.out.println("=============================================");
        Client[] clients = bank.getClients();
        for(int i = 0; i < clients.length; i++){
            if( clients[i] != null)
            {
                System.out.println(clients[i].getAccount().maximumAmountToWithdraw());
            }
        }
    }
}
