package com.luxoft.bankapp.domain.bank;

/**
 * Created by 2 on 12.03.15.
 */
public abstract class AbstractAccount implements Account {

    protected int balance;

    public int getBalance(){
        return balance;
    }

    public final void deposit(int x){
        this.balance += x;
    };
}
