package com.luxoft.bankapp.domain.bank;

/**
 * Created by 2 on 10.03.15.
 */
public class Client {

    //private int balance;

    private Gender gender;

    private String name = "";

    private Account account;

    public void setName(String name) {
        this.name = name;
    }

    /*public Client (String name, Gender gender){
        this.setName(name);
        this.gender = gender;
    }*/

    public Client (String name, Gender gender){
        this.setName(name);
        this.gender = gender;
    }

    public void addAccount(Account account){
        this.account = account;
    }

    public String getClientSalutation(){
       return "Hi, "+this.gender.getSalutation()+" "+this.name;
    }

    public Account getAccount(){
        return account;
    }

    public String getName() {
        return name;
    }
}
