package com.luxoft.bankapp.domain.bank;

/**
 * Created by 2 on 10.03.15.
 */
public class Bank {

    private int countClients = 10;
    private Client[] Clients = new Client[countClients];

   /* public void addClient(Client client){

        for(int i = 0; i < Clients.length; i++){
           if( Clients[i] == null)
           {
               Clients[i] = client;
               break;
           }
        }
    }*/

    public Client[] getClients(){
        return Clients;
    }

    public int getCountClients(){
        return countClients;
    }

}
