package com.luxoft.bankapp.domain.bank;

/**
 * Created by 2 on 10.03.15.
 */
public interface Account {

    public int getBalance();
    public void deposit(int x);
    public void withdraw(int x);
    public int maximumAmountToWithdraw();
    public boolean isOverdraft();
}
