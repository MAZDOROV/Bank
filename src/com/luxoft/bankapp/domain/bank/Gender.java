package com.luxoft.bankapp.domain.bank;

/**
 * Created by 2 on 11.03.15.
 */
public enum Gender {MAIL("Mr."), FEMALE("Ms.");

    public String salutationText;

    private Gender(String salutationText){
        this.salutationText = salutationText;
    }

    public String getSalutation(){
        return salutationText;
    }
};
