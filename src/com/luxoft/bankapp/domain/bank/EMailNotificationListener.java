package com.luxoft.bankapp.domain.bank;

/**
 * Created by Илья on 12.03.2015.
 */
public class EMailNotificationListener implements ClientRegistrationListener {
    public void onClientAdded(Client client){
        System.out.println("Notification email for client" + client.getName() +" to be sent");
    }
}
