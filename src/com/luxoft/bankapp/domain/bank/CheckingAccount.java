package com.luxoft.bankapp.domain.bank;

/**
 * Created by 2 on 12.03.15.
 */
public class CheckingAccount extends AbstractAccount {

    private int overdraft;

    public CheckingAccount(int balance, int overdraft){
        this.overdraft = overdraft;
        this.balance = balance;
    }

    public void withdraw(int x){
        if(x <= maximumAmountToWithdraw()){
            int min = Math.min(x,overdraft);
            balance -= min;
            x -= min;
            overdraft -= x;
        }
    };

    public int maximumAmountToWithdraw(){
        return balance + overdraft;
    };

    public boolean isOverdraft(){
        return true;
    }
}
