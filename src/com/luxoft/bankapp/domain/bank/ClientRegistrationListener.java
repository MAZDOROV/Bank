package com.luxoft.bankapp.domain.bank;

/**
 * Created by Илья on 12.03.2015.
 */
public interface ClientRegistrationListener {
    public void onClientAdded(Client client);
}
