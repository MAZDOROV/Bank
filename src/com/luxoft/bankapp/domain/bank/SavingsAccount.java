package com.luxoft.bankapp.domain.bank;

/**
 * Created by 2 on 12.03.15.
 */
public class SavingsAccount extends AbstractAccount {

    public SavingsAccount(int balance){
        this.balance = balance;
    }

    public void withdraw(int x){
        if(x <= getBalance() ){
            balance -= x;
        }
    };

    public int maximumAmountToWithdraw(){
        return balance;
    };

    public boolean isOverdraft(){
        return false;
    }
}
