package com.luxoft;

import java.util.Random;

import com.luxoft.bankapp.domain.bank.*;
import com.luxoft.bankapp.service.bank.BankService;

/**
 * Created by 2 on 10.03.15.
 */
public class BankApplication {

    public static void main(String[] args){
        BankApplication ba = new BankApplication();
        ba.DoAllWork();
    }

    private void DoAllWork(){
        Bank bank = new Bank();

        /*fillBankClients(bank);
        this.printBalance(bank);*/

        fillClientsWithNames(bank);
        seyHelloForAllClients(bank);

        modifyBank(bank);
        this.printBalance(bank);
        BankService.printMaximumAmountToWithdraw(bank);
    }

    private void fillBankClients(Bank bank) {
        Random random = new Random();
        for(int i = 0; i < bank.getCountClients();i++){
            //bank.addClient(new Client(random.nextInt(500)));
            //BankService.addClient(bank, new Client(random.nextInt(500)));
        }
    }

    public void modifyBank(Bank bank){
        Client[] clients = bank.getClients();
        Random random = new Random();
        for(int i = 0; i < bank.getCountClients(); i++){
            if(i>5){
                clients[i].getAccount().deposit(random.nextInt(500));
            }else{
                clients[i].getAccount().withdraw(random.nextInt(500));
            }
        }
    }

    public void printBalance(Bank bank){
        System.out.println("==============================================");
        Client[] clients = bank.getClients();
        for(int i = 0; i < clients.length;i++)
        {
           System.out.println(clients[i].getAccount().getBalance());
        }
    }

    private void fillClientsWithNames(Bank bank){
        Random random = new Random();
        for(int i = 0; i <bank.getCountClients(); i++ )
        {
            if(i<5){
                //bank.addClient(new Client("Client"+i, Client.Gender.MAIL, random.nextInt(500)));
                Client client = new Client("Client"+i, Gender.MAIL);
                client.addAccount(new CheckingAccount(random.nextInt(500), 100));
                BankService.addClient(bank, client );
            }else {
                //bank.addClient(new Client("Client"+i, Client.Gender.FEMALE, random.nextInt(500)));
                Client client = new Client("Client"+i, Gender.FEMALE);
                client.addAccount(new SavingsAccount(random.nextInt(500)));
                BankService.addClient(bank, client);
            }
        }
    }

     private void seyHelloForAllClients(Bank bank){
         Client[] clients = bank.getClients();
         for (int i = 0; i < clients.length; i++)
         {
            System.out.println(clients[i].getClientSalutation());
         };

     };
}
